const express = require("express");
const app = express();
const path = require("path");

app.use(express.static(path.join(__dirname, "/")));
app.use(express.urlencoded({ extended: false }));

// ROUTES
app.use("/home", (req, res) => {
  res.sendFile(path.join(path.dirname(require.main.filename) + "/index.html"));
});

app.use("/cardapios", (req, res) => {
  res.sendFile(path.dirname(require.main.filename) + "/views/secoes.html");
});

app.use("/menu", (req, res) => {
  res.sendFile(
    path.join(path.dirname(require.main.filename) + "/views/menu.html")
  );
});

app.listen(3000, () => {
  console.log("listening on port 3000");
});
