class Card extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    const src = this.getAttribute("src");
    const iosSrc = this.getAttribute("ios-src");
    const food = this.getAttribute("food");
    const price = this.getAttribute("price");
    const desc = this.getAttribute("desc");

    this.shadowRoot.innerHTML = `
                  <style>
                      #card {
                          margin: 3em auto;
                          display: flex;
                          flex-direction: column;
                          max-width: 300px;
                          border-radius: 6px;
                          box-shadow: 0 3px 10px rgba(0, 0, 0, 0.25);
                          overflow: hidden;
                      }
  
                      model-viewer {
                          width: 100%;
                          height: 400px;
                          background-color: #70bcd1;
                      }
  
                      .attribution {
                          display: flex;
                          flex-direction: row;
                          justify-content: space-between;
                          margin: 1em;
                      }
                        
                      .attribution h1 {
                          margin: 0 0 0.25em;
                      }
                        
                      .attribution img {
                          opacity: 0.5;
                          height: 2em;
                      }
                        
                      .attribution .cc {
                          flex-shrink: 0;
                          text-decoration: none;
                      }
                  </style>
                  <div id="card">
                      <model-viewer
                          src=${src}
                          ios-src="${iosSrc}"
                          alt="${food}"
                          shadow-intensity="1"
                          camera-controls
                          auto-rotate
                          ar
                      ></model-viewer>
                      <section class="attribution">
                          <span>
                              <h1>${food}</h1>
                              <span>Preço: ${price}</span>
                              </br> 
                              <span>Descrição: ${desc} </span>
                          </span>
                      </section>
                  </div>
          `;
  }
}

window.customElements.define("food-card", Card);
