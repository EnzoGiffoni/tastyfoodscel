const foodsP = [
  {
    food: "Cheese",
    src: "../assets/cheese.glb",
    iosSrc: "../assets/Cheese.usdz",
    price: "R$5.00",
    description: "CHEEEEESE",
  },
];

const foodsS = [
  {
    food: "Blueberry Pie",
    src: "../assets/blueberry_crumble_cream_pastry.glb",
    iosSrc: "../assets/Blueberry_Crumble_Cream_Pastly.usdz",
    price: "R$12.00",
    description: "A pie",
  },
  {
    food: "Cupcake",
    src: "../assets/extra_chocolate_marshmallow_cupcake.glb",
    iosSrc: "../assets/Extra_Chocolate_Marshmallow_Cupcake.usdz",
    price: "R$15.00",
    description: "A cupcake",
  },
];

const changeWindow = (selectedMenu) => {
  if (window.location != window.location.origin + "/menu" && selectedMenu) {
    window.location =
      window.location.origin +
      "/menu?selectedMenu=" +
      selectedMenu.toLowerCase();
  }
};

const back = () => {
  window.location = window.location.origin + "/cardapios";
};

const psudoAPI = () => {
  const urlSearchParams = new URLSearchParams(window.location.search);
  const params = Object.fromEntries(urlSearchParams.entries());
  const session = params.selectedMenu;
  console.log(session);
  // request na API
  const div = document.getElementById("row");

  if (session == "principal") {
    foodsP.forEach((item) => {
      const food = item.food;
      const src = item.src;
      const iosSrc = item.src;
      const price = item.price;
      const description = item.description;
      const cardElement = document.createElement("div");
      cardElement.innerHTML = `
      <food-card
        src="${src}"
        ios-src="${iosSrc}"
        food="${food}"
        price="${price}"
        desc="${description}"
      ></food-card>
      `;
      div.appendChild(cardElement);
    });
  }

  if (session == "sobremesas") {
    foodsS.forEach((item) => {
      const food = item.food;
      const src = item.src;
      const iosSrc = item.src;
      const price = item.price;
      const description = item.description;
      const cardElement = document.createElement("div");
      cardElement.innerHTML = `
      <food-card
        src="${src}"
        ios-src="${iosSrc}"
        food="${food}"
        price="${price}"
        desc="${description}"
      ></food-card>
      `;
      div.appendChild(cardElement);
    });
  }
};

const title = () => {
  const oi = document.getElementById("title");
  const urlSearchParams = new URLSearchParams(window.location.search);
  const params = Object.fromEntries(urlSearchParams.entries());
  const session = params.selectedMenu;
  const Title = captalizeFirstLetter(session);
  oi.innerText = Title;
};

const captalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};
