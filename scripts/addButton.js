const mockData = ["Principal", "Sobremesas"];

const buildButton = () => {
  const div = document.getElementById("row");
  const response = [];

  mockData.forEach((menu) => {
    const menuElement = document.createElement("div");
    menuElement.innerHTML = `
      <button class="button-28" onclick="changeWindow('${menu.replace(
        /\s/g,
        ""
      )}')">${menu}</button>
    `;
    div.appendChild(menuElement);
  });
};
