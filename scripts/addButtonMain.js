const mockDataMain = ["Normal", "Kids"];

const backHome = () => {
  window.location = window.location.origin + "/home";
};

const buildButtonMain = () => {
  const divMain = document.getElementById("row");
  mockDataMain.forEach((cardapio) => {
    const cardapioElement = document.createElement("div");
    cardapioElement.innerHTML = `<button class="button-28" onclick="goToCardapio('${cardapio.replace(
      /\s/g,
      ""
    )}')">${cardapio}</button>`;
    divMain.appendChild(cardapioElement);
  });
};

const goToCardapio = (selectedCardapio) => {
  window.location =
    window.location.origin +
    "/cardapios?selectedCardapio=" +
    selectedCardapio.toLowerCase();
};
